package br.com.provatecnicaapi.runners;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.provatecnicaapi.tests.restricoes.tests.GetConsultaSeUmCPFPossuiOuNaoRestricaoTest;
import br.com.provatecnicaapi.tests.simulacoes.tests.GetRetornaTodasAsSimulacoesExistentesTest;
import br.com.provatecnicaapi.tests.simulacoes.tests.GetRetornaUmaSimulacaoAtravesDoCPFTest;
import br.com.provatecnicaapi.tests.simulacoes.tests.PostInsereUmaNovaSimulacaoTest;
import br.com.provatecnicaapi.tests.simulacoes.tests.PutAtualizaUmaSimulacaoExistenteAtravesDoCPFTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(br.com.provatecnicaapi.suites.AllTests.class)
@Suite.SuiteClasses({
        GetConsultaSeUmCPFPossuiOuNaoRestricaoTest.class,
        GetRetornaTodasAsSimulacoesExistentesTest.class,
        PostInsereUmaNovaSimulacaoTest.class,
        GetRetornaUmaSimulacaoAtravesDoCPFTest.class,
        PutAtualizaUmaSimulacaoExistenteAtravesDoCPFTest.class
})

/*Classe responsável por armazenar todos as categorias de testes. */
public class AllTests {
}
