package br.com.provatecnicaapi.tests.simulacoes.requests.payloads.cadastros;

import java.util.Locale;

import com.github.javafaker.Faker;

/*Classe responsável por armazenar dados cadastrais das simulações. */
public class CadastrosDasSimulacoes {

    Faker faker = new Faker(new Locale("pt-BR"));

    private String nomePessoa = faker.address().firstName();
    private String cpfDuplicado = "12493124774";
    private String cpfNaoCadastrado = "21239875874";
    private long cpfValorLong = faker.number().randomNumber(11, true);
    private String cpfAleatorio = faker.number().digits(11);
    private String emailPessoa = faker.internet().emailAddress();
    private String emailInvalido = "emailinvaldo";
    private int valorSimulacao = faker.number().numberBetween(5, 40000);
    private int valorSimulacaoMaiorQueQuarentaMil = faker.number().numberBetween(7, 4000000);
    private int qtParcelas = faker.number().numberBetween(2, 48);
    private int qtParcelasIgualAUm = 1;
    private boolean seguroNaSimulacao = faker.bool().bool();

    /*Método construtor da classe de CadastrosDasSimulacoes. */
    public CadastrosDasSimulacoes() {

    }

    /**
     * @return : Retorna o nome da pessoa.
     */
    public String getNomePessoa() {
        return this.nomePessoa;
    }

    /**
     * @param nomePessoa : Permite alterar o nome da pessoa.
     */
    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    /**
     * @return : Retorna cpf duplicado.
     */
    public String getCpfDuplicado() {
        return this.cpfDuplicado;
    }

    /**
     * @param cpfDuplicado : Permite alterar cpf duplicado.
     */
    public void setCpfDuplicado(String cpfDuplicado) {
        this.cpfDuplicado = cpfDuplicado;
    }
    
    /**
     * @return : Retorna e-mail da pessoa.
     */
    public String getEmailPessoa() {
        return this.emailPessoa;
    }

    /**
     * @param emailPessoa : Permite alterar e-mail da pessoa.
     */
    public void setEmailPessoa(String emailPessoa) {
        this.emailPessoa = emailPessoa;
    }

    /**
     * @return : Retorna e-mail inválido.
     */
    public String getEmailInvalido() {
        return this.emailInvalido;
    }

    /**
     * @param emailInvalido : Permite alterar e-mail inválido.
     */
    public void setEmailInvalido(String emailInvalido) {
        this.emailInvalido = emailInvalido;
    }

    /**
     * @return : Retorna um valor na simulação.
     */
    public Integer getValorSimulacao() {
        return this.valorSimulacao;
    }

    /**
     * @param valorSimulacao : Permite alterar o valor na simulação.
     */
    public void setValorSimulacao(Integer valorSimulacao) {
        this.valorSimulacao = valorSimulacao;
    }

    /**
     * @return : Retorna o valor da simulação maior que quarenta mil.
     */
    public Integer getValorSimulacaoMaiorQueQuarentaMil() {
        return this.valorSimulacaoMaiorQueQuarentaMil;
    }

    /**
     * @param valorSimulacaoMaiorQueQuarentaMil : Permite alterar o valor da simulação maior que quarenta mil.
     */
    public void setValorSimulacaoMaiorQueQuarentaMil(Integer valorSimulacaoMaiorQueQuarentaMil) {
        this.valorSimulacaoMaiorQueQuarentaMil = valorSimulacaoMaiorQueQuarentaMil;
    }

    /**
     * @return : Retorna a quantidade de parcelas.
     */
    public Integer getQtParcelas() {
        return this.qtParcelas;
    }

    /**
     * @param qtParcelas : Permite alterar a quantidade de parcelas.
     */
    public void setQtParcelas(Integer qtParcelas) {
        this.qtParcelas = qtParcelas;
    }

    /**
     * @return : Retorna a quantidade de parcelas igual a um.
     */
    public Integer getQtParcelasIgualAUm() {
        return this.qtParcelasIgualAUm;
    }

    /**
     * @param qtParcelasIgualAUm : Permite alterar a quantidade de parcelas igual a um.
     */
    public void getQtParcelasIgualAUm(Integer qtParcelasIgualAUm) {
        this.qtParcelasIgualAUm = qtParcelasIgualAUm;
    }

    /**
     * @return : Retorna valor boolean informando se tem ou não seguro na simulação.
     */
    public boolean getSeguroNaSimulacao() {
        return this.seguroNaSimulacao;
    }

    /**
     * @param seguroNaSimulacao : Permite alterar o valor boolean informando se tem ou não seguro na simulação.
     */
    public void setSeguroNaSimulacao(boolean seguroNaSimulacao) {
        this.seguroNaSimulacao = seguroNaSimulacao;
    }

    /**
     * @return : Retorna cpf não cadastrado.
     */
    public String getCpfNaoCadastrado() {
        return this.cpfNaoCadastrado;
    }

    /**
     * @param cpfNaoCadastrado : Permite alterar cpf não cadastrado.
     */
    public void setCpfNaoCadastrado(String cpfNaoCadastrado) {
        this.cpfNaoCadastrado = cpfNaoCadastrado;
    }

    /**
     * @return : Retorna cpf com valor long.
     */
    public long getCpfValorLong() {
        return this.cpfValorLong;
    }

    /**
     * @param cpfValorLong : Permite alterar cpf com valor long.
     */
    public void setcpfValorLong(long cpfValorLong) {
        this.cpfValorLong = cpfValorLong;
    }
    
    /**
     * @return : Retorna um cpf aleatório.
     */
    public String getCpfAleatorio() {
        return cpfAleatorio;
    }

    /**
     * @param cpfAleatorio : Permite alterar cpf aleatório.
     */
    public void setCpfAleatorio(String cpfAleatorio) {
        this.cpfAleatorio = cpfAleatorio;
    }
}
