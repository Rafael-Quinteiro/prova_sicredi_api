package br.com.provatecnicaapi.tests.simulacoes.requests.payloads;

import org.json.JSONObject;

import br.com.provatecnicaapi.tests.simulacoes.requests.payloads.cadastros.CadastrosDasSimulacoes;

/*Classe responsável por armazenar os dados de payload. */
public class AtualizaUmaSimulacaoExistenteAtravésDoCPFPayloads {

    /*Instancia de CadastrosDasSimulacoes. */
    CadastrosDasSimulacoes cadastrosDasSimulacoes = new CadastrosDasSimulacoes();
    
    /**
     * Método responsável por atualizar dados válidos no payload para uma simulação existente.
     * @return : Retorna o payload com dados para atualizar simulação existente.
     */
    public JSONObject jsonAtualizaUmaSimulacaoExistenteAtravesDoCPF() {
        JSONObject payloadAtualizaUmaSimulacaoExisteAtravesDoCPF = new JSONObject();

        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("cpf", cadastrosDasSimulacoes.getCpfDuplicado());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("email", cadastrosDasSimulacoes.getEmailPessoa());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadAtualizaUmaSimulacaoExisteAtravesDoCPF;
    }

    /**
     * Método responsável por atualizar dados válidos no payload para uma simulação não existente.
     * @return : Retorna o payload com dados para atualizar simulação não existente.
     */
    public JSONObject jsonAtualizaUmaSimulacaoNaoExistente() {
        JSONObject payloadAtualizaUmaSimulacaoExisteAtravesDoCPF = new JSONObject();

        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("cpf", cadastrosDasSimulacoes.getCpfNaoCadastrado());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("email", cadastrosDasSimulacoes.getEmailPessoa());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadAtualizaUmaSimulacaoExisteAtravesDoCPF.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadAtualizaUmaSimulacaoExisteAtravesDoCPF;
    }
}
