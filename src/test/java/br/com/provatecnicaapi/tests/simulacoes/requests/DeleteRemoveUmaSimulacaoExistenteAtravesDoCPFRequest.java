package br.com.provatecnicaapi.tests.simulacoes.requests;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;

/*Classe responsável por armazenar os dados de request. */
public class DeleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest {
    
    /**
     * Método responsável pela request que deleta uma simulação através do cpf.
     * @param idsimulacao : Permite inserir o id da simulação.
     * @return : Retorna dados da estrutura do request.
     */
    public Response deletaUmaSimulacaoAtravesDoCPFRequest(int idSimulacao) {
        return given()
                .when()
                .delete("api/v1/simulacoes/"+ idSimulacao);
    }
}
