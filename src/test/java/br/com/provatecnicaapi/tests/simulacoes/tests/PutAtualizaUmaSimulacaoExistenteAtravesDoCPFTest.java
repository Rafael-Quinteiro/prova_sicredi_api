package br.com.provatecnicaapi.tests.simulacoes.tests;

import static org.hamcrest.CoreMatchers.is;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import br.com.provatecnicaapi.base.BaseTest;
import br.com.provatecnicaapi.suites.AllTests;
import br.com.provatecnicaapi.tests.simulacoes.requests.PutAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest;

/*Classe de testes de atualizar uma simulação. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PutAtualizaUmaSimulacaoExistenteAtravesDoCPFTest extends BaseTest {

    private String cpfDuplicado = "12493124774";
    private String cpfNaoCadastrado = "21239875874";

    /*Instância de PutAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest. */ 
    PutAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest putAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest = new PutAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest();

    /*Método de teste responsável por alterar uma nova simulação. */
    @Test
    @Category({AllTests.class})
    public void TC001_deveAlterarSimulacaoCadastrada() {

        putAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest.atualizaUmaSimulacaoExistenteAtravesDoCPFRequest()
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("cpf", is(cpfDuplicado));
    }

    /*Método de teste responsável por não petmirir alterar uma simulação que não esteja cadastrada. */
    @Test
    @Category({AllTests.class})
    public void TC002_naoDeveAlterarSimulacaoQueNaoEstejaCadastrada() {

        putAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest.atualizaUmaSimulacaoNaoExistenteRequest()
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem", is("CPF " + cpfNaoCadastrado + " não encontrado"));
    }
}
