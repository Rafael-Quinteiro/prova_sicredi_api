package br.com.provatecnicaapi.tests.simulacoes.tests;

import static org.hamcrest.Matchers.greaterThan;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import br.com.provatecnicaapi.base.BaseTest;
import br.com.provatecnicaapi.suites.AllTests;
import br.com.provatecnicaapi.tests.simulacoes.requests.GetRetornaTodasAsSimulacoesExistentesRequest;

/*Classe de testes de retornar todas as simulações existentes. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GetRetornaTodasAsSimulacoesExistentesTest extends BaseTest {

    /*Instância de GetRetornaTodasAsSimulacoesExistentesRequest. */ 
    GetRetornaTodasAsSimulacoesExistentesRequest getRetornaTodasAsSimulacoesExistesRequest = new GetRetornaTodasAsSimulacoesExistentesRequest();

    /*Método de teste responsável por consultar todas as simulações existentes. */
    @Test
    @Category({AllTests.class})
    public void TC001_deveConsultarTodasAsSimulacoesExistentes() {

        getRetornaTodasAsSimulacoesExistesRequest.retornaTodasAsSimulacoesExistentesRequest()
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("size()", greaterThan(0));
    }
}
