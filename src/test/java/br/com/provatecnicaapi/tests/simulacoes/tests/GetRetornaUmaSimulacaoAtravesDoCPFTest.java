package br.com.provatecnicaapi.tests.simulacoes.tests;

import static org.hamcrest.CoreMatchers.is;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import br.com.provatecnicaapi.base.BaseTest;
import br.com.provatecnicaapi.suites.AllTests;
import br.com.provatecnicaapi.tests.simulacoes.requests.GetRetornaUmaSimulacaoAtravesDoCPFRequest;
import br.com.provatecnicaapi.tests.simulacoes.requests.payloads.cadastros.CadastrosDasSimulacoes;

/*Classe de testes de retornar uma simulação através do cpf. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GetRetornaUmaSimulacaoAtravesDoCPFTest extends BaseTest {
    
    /*Instâncias de GetRetornaTodasAsSimulacoesExistentesRequest e CadastrosDasSimulacoes. */ 
    GetRetornaUmaSimulacaoAtravesDoCPFRequest getRetornaUmaSimulacaoAtravesDoCPFRequest = new GetRetornaUmaSimulacaoAtravesDoCPFRequest();
    CadastrosDasSimulacoes cadastros = new CadastrosDasSimulacoes();

    /*Método de teste responsável por não permitir consultar uma simulação para cpf que não esteja cadastrado. */
    @Test
    @Category({AllTests.class})
    public void TC001_naoDeveSerPossivelConsultarSimulacaoComCPFNaoCadastrado() {

        getRetornaUmaSimulacaoAtravesDoCPFRequest.retornaUmaSimulacaoAtravesDoCPFRequest(cadastros.getCpfNaoCadastrado())
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem", is("CPF " + cadastros.getCpfNaoCadastrado() + " não encontrado"));
    }

    /*Método de teste responsável por consultar uma simulação através do cpf. */
    @Test
    @Category({AllTests.class})
    public void TC002_deveSerPossivelConsultarSimulacaoComCPFCadastrado() {

        getRetornaUmaSimulacaoAtravesDoCPFRequest.retornaUmaSimulacaoAtravesDoCPFRequest(cadastros.getCpfDuplicado())
                .then()
                .statusCode(HttpStatus.SC_OK);
    }                
}
