package br.com.provatecnicaapi.tests.simulacoes.requests;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;

/*Classe responsável por armazenar os dados de request. */
public class GetRetornaUmaSimulacaoAtravesDoCPFRequest {

    /**
     * Método responsável pela request que retorna uma simulação através do cpf.
     * @param cpf : Permite inserir o cpf para a simulação.
     * @return : Retorna dados da estrutura do request.
     */
    public Response retornaUmaSimulacaoAtravesDoCPFRequest(String cpf) {
        return given()
                .when()
                .get("api/v1/simulacoes/"+ cpf);
    }
}
