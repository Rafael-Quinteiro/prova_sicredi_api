package br.com.provatecnicaapi.tests.simulacoes.requests;

import static io.restassured.RestAssured.given;

import br.com.provatecnicaapi.tests.simulacoes.requests.payloads.InsereUmaNovaSimulacaoPayloads;
import io.restassured.response.Response;

/*Classe responsável por armazenar os dados de request. */
public class PostInsereUmaNovaSimulacaoRequest {

    /*Instancia de InsereUmaNovaSimulacaoPayloads. */
    InsereUmaNovaSimulacaoPayloads insereUmaNovaSimulacaoPayloads = new InsereUmaNovaSimulacaoPayloads();
    
    /**
     * Método responsável pela request que insere uma nova simulação.
     * @return : Retorna dados da estrutura do request.
     */
    public Response insereUmaNovaSimulacaoRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonInsereUmaNovaSimulacao().toString())
                .post("api/v1/simulacoes");  
    }

    /**
     * Método responsável pela request que insere uma nova simulação com cpf contendo valor long.
     * @return : Retorna dados da estrutura do request.
     */
    public Response insereUmaNovaSimulacaoRequestComCpfValorLongRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonInsereUmaNovaSimulacaoComCpfValorLong().toString())
                .post("api/v1/simulacoes");  
    }

    /**
     * Método responsável pela request que insere uma nova simulação com cpf já cadastrado.
     * @return : Retorna dados da estrutura do request.
     */
    public Response naoDeveInserirSimulacaoParaCpfJaCadastradoRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonNaoDeveInserirSimulacaoParaCpfJaCadastrado().toString())
                .post("api/v1/simulacoes");  
    }

    /**
     * Método responsável pela request que insere uma nova simulação sem informar payload.
     * @return : Retorna dados da estrutura do request.
     */
    public Response naoDeveInserirSimulacaoSemInformarPayloadRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonNaoDeveInserirSimulacaoSemInformarPayload().toString())
                .post("api/v1/simulacoes");  
    }

    /**
     * Método responsável pela request que insere uma nova simulação com e-mail inválido.
     * @return : Retorna dados da estrutura do request.
     */
    public Response naoDeveInserirSimulacaoComEmailInvalidoRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonNaoDeveInserirSimulacaoComEmailInvalido().toString())
                .post("api/v1/simulacoes");  
    }

    /**
     * Método responsável pela request que insere uma nova simulação com valor maior que quarenta mil.
     * @return : Retorna dados da estrutura do request.
     */
    public Response naoDeveInserirSimulacaoComValorMaiorQueQuarentaMilRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonNaoDeveInserirSimulacaoComValorMaiorQueQuarentaMil().toString())
                .post("api/v1/simulacoes");  
    }

    /**
     * Método responsável pela request que insere uma nova simulação com parcela menor que dois.
     * @return : Retorna dados da estrutura do request.
     */
    public Response naoDeveInserirSimulacaoComParcelaMenorQueDoisRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(insereUmaNovaSimulacaoPayloads.jsonNaoDeveInserirSimulacaoComParcelaMenorQueDoisRequest().toString())
                .post("api/v1/simulacoes");  
    }
}
