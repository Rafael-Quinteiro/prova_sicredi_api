package br.com.provatecnicaapi.tests.simulacoes.tests;

import static org.hamcrest.CoreMatchers.is;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import br.com.provatecnicaapi.base.BaseTest;
import br.com.provatecnicaapi.suites.AllTests;
import br.com.provatecnicaapi.tests.simulacoes.requests.PostInsereUmaNovaSimulacaoRequest;

/*Classe de testes de inseir uma simulação. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PostInsereUmaNovaSimulacaoTest extends BaseTest {

    private String cpfDuplicado = "CPF duplicado";
    private String qtParcelas = "Parcelas não pode ser vazio";
    private String nrCpf = "CPF não pode ser vazio";
    private String valorSimulacao = "Valor não pode ser vazio";
    private String nomePessoa = "Nome não pode ser vazio";
    private String emailPessoa = "E-mail não deve ser vazio";
    private String emailPessoaInvalido = "E-mail deve ser um e-mail válido";
    private String valorSimulacaoMaiorQueQuarentaMil = "Valor deve ser menor ou igual a R$ 40.000";
    private String qtParcelasIgualAUm = "Parcelas deve ser igual ou maior que 2";
    
    /*Instância de PostInsereUmaNovaSimulacaoRequest. */ 
    PostInsereUmaNovaSimulacaoRequest postInsereUmaNovaSimulacaoRequest = new PostInsereUmaNovaSimulacaoRequest();

    /*Método de teste responsável por inserir uma nova simulação. */
    @Test
    @Category({AllTests.class})
    public void TC001_deveInserirUmaSimulacao() {

        postInsereUmaNovaSimulacaoRequest.insereUmaNovaSimulacaoRequest()
                .then()
                .statusCode(HttpStatus.SC_CREATED);
    }

    /*Método de teste responsável por inserir uma nova simulação inserindo cpf como long. */
    @Test
    @Category({AllTests.class})
    public void TC002_deveInserirUmaSimulacaoEnviandoCpfComoLong() {

        postInsereUmaNovaSimulacaoRequest.insereUmaNovaSimulacaoRequestComCpfValorLongRequest()
                .then()
                .statusCode(HttpStatus.SC_CREATED);
    }

    /*Método de teste responsável por não permitir inserir uma nova simulação inserindo cpf já esteja cadastrado. */
    @Test
    @Category({AllTests.class})
    public void TC003_naoDeveInserirSimulacaoParaCpfJaCadastrado() {

        postInsereUmaNovaSimulacaoRequest.naoDeveInserirSimulacaoParaCpfJaCadastradoRequest()
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("mensagem", is(cpfDuplicado));
    }

    /*Método de teste responsável não por permitir inserir uma nova simulação sem informar dados de payload. */
    @Test
    @Category({AllTests.class})
    public void TC004_naoDeveInserirSimulacaoSemInformarPayload() {

        postInsereUmaNovaSimulacaoRequest.naoDeveInserirSimulacaoSemInformarPayloadRequest()
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.parcelas", is(qtParcelas))
                .body("erros.cpf", is(nrCpf))
                .body("erros.valor", is(valorSimulacao))
                .body("erros.nome", is(nomePessoa))
                .body("erros.email", is(emailPessoa));

    }

    /*Método de teste responsável não por permitir inserir uma nova simulação inserindo e-mail inválido. */
    @Test
    @Category({AllTests.class})
    public void TC005_naoDeveSerPossivelInserirSimulacaoComEmailInvalido() {

        postInsereUmaNovaSimulacaoRequest.naoDeveInserirSimulacaoComEmailInvalidoRequest()
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.email", is(emailPessoaInvalido));
    }

    /*Método de teste responsável não por permitir inserir uma nova simulação inserindo valor de simulação maior que quarenta mil. */
    @Test
    @Category({AllTests.class})
    public void TC006_naoDeveSerPossivelInserirSimulacaoComValorMaiorQueQuarentaMil() {

        postInsereUmaNovaSimulacaoRequest.naoDeveInserirSimulacaoComValorMaiorQueQuarentaMilRequest()
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.valor", is(valorSimulacaoMaiorQueQuarentaMil));
    }

    /*Método de teste responsável não por permitir inserir uma nova simulação inserindo parcela menor que dois. */
    @Test
    @Category({AllTests.class})
    public void TC007_naoDeveInserirSimulacaoInformandoUmaParcela() {

        postInsereUmaNovaSimulacaoRequest.naoDeveInserirSimulacaoComParcelaMenorQueDoisRequest()
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.parcelas", is(qtParcelasIgualAUm));
    }
}