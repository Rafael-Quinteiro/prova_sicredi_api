package br.com.provatecnicaapi.tests.simulacoes.requests;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;

/*Classe responsável por armazenar os dados de request. */
public class GetRetornaTodasAsSimulacoesExistentesRequest {
    
    /**
     * Método responsável pela request que retorna todas as simulações existentes.
     * @return : Retorna dados da estrutura do request.
     */
    public Response retornaTodasAsSimulacoesExistentesRequest() {
        return given()
                .when()
                .get("api/v1/simulacoes/");
    }
}
