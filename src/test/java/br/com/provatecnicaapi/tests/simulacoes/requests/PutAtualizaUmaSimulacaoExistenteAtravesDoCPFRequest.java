package br.com.provatecnicaapi.tests.simulacoes.requests;

import static io.restassured.RestAssured.given;

import br.com.provatecnicaapi.tests.simulacoes.requests.payloads.AtualizaUmaSimulacaoExistenteAtravésDoCPFPayloads;
import br.com.provatecnicaapi.tests.simulacoes.requests.payloads.cadastros.CadastrosDasSimulacoes;
import io.restassured.response.Response;

/*Classe responsável por armazenar os dados de request. */
public class PutAtualizaUmaSimulacaoExistenteAtravesDoCPFRequest {
    
    /*Instâncias de AtualizaUmaSimulacaoExistenteAtravésDoCPFPayloads e CadastrosDasSimulacoes. */ 
    AtualizaUmaSimulacaoExistenteAtravésDoCPFPayloads atualizaUmaSimulacaoExistenteAtravésDoCPFPayloads = new AtualizaUmaSimulacaoExistenteAtravésDoCPFPayloads();
    CadastrosDasSimulacoes cadastrosDasSimulacoes = new CadastrosDasSimulacoes();

    /**
     * Método responsável pela request que atualiza uma simulação existente através do cpf.
     * @return : Retorna dados da estrutura do request.
     */
    public Response atualizaUmaSimulacaoExistenteAtravesDoCPFRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(atualizaUmaSimulacaoExistenteAtravésDoCPFPayloads.jsonAtualizaUmaSimulacaoExistenteAtravesDoCPF().toString())
                .put("api/v1/simulacoes/"+ cadastrosDasSimulacoes.getCpfDuplicado()); 
    }

    /**
     * Método responsável pela request que atualiza uma simulação não existente através do cpf.
     * @return : Retorna dados da estrutura do request.
     */
    public Response atualizaUmaSimulacaoNaoExistenteRequest() {
        return given()
                .header("Content-Type","application/json")
                .when()
                .body(atualizaUmaSimulacaoExistenteAtravésDoCPFPayloads.jsonAtualizaUmaSimulacaoNaoExistente().toString())
                .put("api/v1/simulacoes/"+ cadastrosDasSimulacoes.getCpfNaoCadastrado()); 
    }
}
