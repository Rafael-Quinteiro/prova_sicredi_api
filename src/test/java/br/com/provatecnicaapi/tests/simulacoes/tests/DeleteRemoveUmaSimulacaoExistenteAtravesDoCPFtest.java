package br.com.provatecnicaapi.tests.simulacoes.tests;

import static org.hamcrest.CoreMatchers.is;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import br.com.provatecnicaapi.base.BaseTest;
import br.com.provatecnicaapi.suites.AllTests;
import br.com.provatecnicaapi.tests.simulacoes.requests.DeleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest;

/*Classe de testes de deletar uma simulação. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeleteRemoveUmaSimulacaoExistenteAtravesDoCPFtest extends BaseTest{

    int idSimulacao  = 13;
    int idSimulacaoNaoEncontrada  = 200;
    String mensagemSimulacaoNaoEncontrada = "Simulação não encontrada";
    
    /*Instância de DeleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest. */ 
    DeleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest deleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest = new DeleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest();

    /*Método de teste responsável por deletar uma simulação cadastrada. */
    @Test
    @Category({AllTests.class})
    public void TC001_deveRealizarExclusaoDeSimulacaoCadastrada() {

        deleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest.deletaUmaSimulacaoAtravesDoCPFRequest(idSimulacao)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    /*Método de teste responsável por não deletar uma simulação que não esteja cadastrada. */
    @Test
    @Category({AllTests.class})
    public void TC002_naoDeveRealizarExclusaoDeSimulacaoNaoCadastrada() {

        deleteRemoveUmaSimulacaoExistenteAtravesDoCPFRequest.deletaUmaSimulacaoAtravesDoCPFRequest(idSimulacaoNaoEncontrada)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem", is (mensagemSimulacaoNaoEncontrada));
    }
}
