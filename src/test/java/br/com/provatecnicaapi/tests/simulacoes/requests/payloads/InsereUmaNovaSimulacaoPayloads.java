package br.com.provatecnicaapi.tests.simulacoes.requests.payloads;

import org.json.JSONObject;

import br.com.provatecnicaapi.tests.simulacoes.requests.payloads.cadastros.CadastrosDasSimulacoes;

/*Classe responsável por armazenar os dados de payload. */
public class InsereUmaNovaSimulacaoPayloads {
    
    /*Instancia de CadastrosDasSimulacoes. */
    CadastrosDasSimulacoes cadastrosDasSimulacoes = new CadastrosDasSimulacoes();

    /**
     * Método responsável por inserir dados do payload para uma nova simulação.
     * @return : Retorna o payload com dados para uma nova simulação.
     */
    public JSONObject jsonInsereUmaNovaSimulacao() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        payloadInsereUmaNovaSimulacao.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadInsereUmaNovaSimulacao.put("cpf", cadastrosDasSimulacoes.getCpfDuplicado());
        payloadInsereUmaNovaSimulacao.put("email", cadastrosDasSimulacoes.getEmailPessoa());
        payloadInsereUmaNovaSimulacao.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadInsereUmaNovaSimulacao.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadInsereUmaNovaSimulacao.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadInsereUmaNovaSimulacao;
    }

    /**
     * Método responsável por inserir dados do payload para uma nova simulação com cpf contendo valor long.
     * @return : Retorna o payload com dados para uma nova simulação contendo valor long.
     */
    public JSONObject jsonInsereUmaNovaSimulacaoComCpfValorLong() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        payloadInsereUmaNovaSimulacao.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadInsereUmaNovaSimulacao.put("cpf", cadastrosDasSimulacoes.getCpfValorLong());
        payloadInsereUmaNovaSimulacao.put("email", cadastrosDasSimulacoes.getEmailPessoa());
        payloadInsereUmaNovaSimulacao.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadInsereUmaNovaSimulacao.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadInsereUmaNovaSimulacao.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadInsereUmaNovaSimulacao;
    }

    /**
     * Método responsável por inserir dados do payload para uma simulação com cpf não cadastrado.
     * @return : Retorna o payload com dados para uma simulação com cpf não cadastrado.
     */
    public JSONObject jsonNaoDeveInserirSimulacaoParaCpfJaCadastrado() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        payloadInsereUmaNovaSimulacao.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadInsereUmaNovaSimulacao.put("cpf", cadastrosDasSimulacoes.getCpfDuplicado());
        payloadInsereUmaNovaSimulacao.put("email", cadastrosDasSimulacoes.getEmailPessoa());
        payloadInsereUmaNovaSimulacao.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadInsereUmaNovaSimulacao.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadInsereUmaNovaSimulacao.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadInsereUmaNovaSimulacao;
    }

    /**
     * Método responsável por não inserir dados do payload.
     * @return : Retorna o payload sem dados.
     */
    public JSONObject jsonNaoDeveInserirSimulacaoSemInformarPayload() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        return payloadInsereUmaNovaSimulacao;
    }

    /**
     * Método responsável por inserir dados do payload para uma simulação com e-mail inválido.
     * @return : Retorna o payload com dados para uma simulação com e-mail inválido.
     */
    public JSONObject jsonNaoDeveInserirSimulacaoComEmailInvalido() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        payloadInsereUmaNovaSimulacao.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadInsereUmaNovaSimulacao.put("cpf", cadastrosDasSimulacoes.getCpfDuplicado());
        payloadInsereUmaNovaSimulacao.put("email", cadastrosDasSimulacoes.getEmailInvalido());
        payloadInsereUmaNovaSimulacao.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadInsereUmaNovaSimulacao.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadInsereUmaNovaSimulacao.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadInsereUmaNovaSimulacao;
    }

    /**
     * Método responsável por inserir dados do payload para uma simulação com valor maior que quarenta mil.
     * @return : Retorna o payload com dados para uma simulação com valor maior que quarenta mil.
     */
    public JSONObject jsonNaoDeveInserirSimulacaoComValorMaiorQueQuarentaMil() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        payloadInsereUmaNovaSimulacao.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadInsereUmaNovaSimulacao.put("cpf", cadastrosDasSimulacoes.getCpfDuplicado());
        payloadInsereUmaNovaSimulacao.put("email", cadastrosDasSimulacoes.getEmailInvalido());
        payloadInsereUmaNovaSimulacao.put("valor", cadastrosDasSimulacoes.getValorSimulacaoMaiorQueQuarentaMil());
        payloadInsereUmaNovaSimulacao.put("parcelas", cadastrosDasSimulacoes.getQtParcelas());
        payloadInsereUmaNovaSimulacao.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadInsereUmaNovaSimulacao;
    }

    /**
     * Método responsável por inserir dados do payload para uma simulação com parcela menor que dois.
     * @return : Retorna o payload com dados para uma simulação com parcela menor que dois.
     */
    public JSONObject jsonNaoDeveInserirSimulacaoComParcelaMenorQueDoisRequest() {
        JSONObject payloadInsereUmaNovaSimulacao = new JSONObject();

        payloadInsereUmaNovaSimulacao.put("nome", cadastrosDasSimulacoes.getNomePessoa());
        payloadInsereUmaNovaSimulacao.put("cpf", cadastrosDasSimulacoes.getCpfAleatorio());
        payloadInsereUmaNovaSimulacao.put("email", cadastrosDasSimulacoes.getEmailPessoa());
        payloadInsereUmaNovaSimulacao.put("valor", cadastrosDasSimulacoes.getValorSimulacao());
        payloadInsereUmaNovaSimulacao.put("parcelas", cadastrosDasSimulacoes.getQtParcelasIgualAUm());
        payloadInsereUmaNovaSimulacao.put("seguro", cadastrosDasSimulacoes.getSeguroNaSimulacao());

        return payloadInsereUmaNovaSimulacao;
    }
}
