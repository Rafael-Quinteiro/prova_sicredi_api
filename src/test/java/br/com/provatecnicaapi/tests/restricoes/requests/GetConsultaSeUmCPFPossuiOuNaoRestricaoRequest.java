package br.com.provatecnicaapi.tests.restricoes.requests;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;

/*Classe responsável por armazenar os dados de request. */
public class GetConsultaSeUmCPFPossuiOuNaoRestricaoRequest {

    /**
     * Método responsável pela request que retorna se um cpf possuí o não restrições.
     * @param cpf : Permite inserir o cpf que possuí ou não restrição.
     * @return : Retorna dados da estrutura do request.
     */
    public Response retornaSeUmCPFPossuiOuNaoRestricaoRequest(String cpf) {
        return given()
                    .when()
                    .get("api/v1/restricoes/" + cpf);
    }
}
