package br.com.provatecnicaapi.tests.restricoes.tests.cadastros;

/*Classe responsável por armazenar dados cadastrais das restrições. */
public class CadastrosDasRestricoes {

    private String cpfComRestricao = "24094592008";
    private String cpfSemRestricao = "12493124774";

    /*Método construtor da classe de CadastrosDasRestricoes. */
    public CadastrosDasRestricoes() {
    }
    
    /**
     * @return : Retorna cpf com restrição.
     */
    public String getcpfComRestricao() {
        return this.cpfComRestricao;
    }

    /**
     * @param cpfComRestricao : Permite alterar o cpf com restrição.
     */
    public void setcpfComRestricao(String cpfComRestricao) {
        this.cpfComRestricao = cpfComRestricao;
    }

    /**
     * @return : Retorna cpf sem restrição.
     */
    public String getcpfSemRestricao() {
        return this.cpfSemRestricao;
    }

    /**
     * @param cpfSemRestricao : Permite alterar o cpf sem restrição.
     */
    public void setcpfSemRestricao(String cpfSemRestricao) {
        this.cpfSemRestricao = cpfSemRestricao;
    }
}
