package br.com.provatecnicaapi.tests.restricoes.tests;

import static org.hamcrest.CoreMatchers.is;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import br.com.provatecnicaapi.base.BaseTest;
import br.com.provatecnicaapi.suites.AllTests;
import br.com.provatecnicaapi.tests.restricoes.requests.GetConsultaSeUmCPFPossuiOuNaoRestricaoRequest;
import br.com.provatecnicaapi.tests.restricoes.tests.cadastros.CadastrosDasRestricoes;

/*Classe de testes de consulta se cpf possui ou não restrição. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GetConsultaSeUmCPFPossuiOuNaoRestricaoTest extends BaseTest {

    /*Instâncias de GetConsultaSeUmCPFPossuiOuNaoRestricaoRequest e CadastrosDasRestricoes. */ 
    GetConsultaSeUmCPFPossuiOuNaoRestricaoRequest getConsultaSeUmCPFPossuiOuNaoRestricaoRequest = new GetConsultaSeUmCPFPossuiOuNaoRestricaoRequest();
    CadastrosDasRestricoes cadastros = new CadastrosDasRestricoes();

    /*Método de teste responsável por consultar uma cpf com restrição. */
    @Test
    @Category({AllTests.class})
    public void TC001_deveConsultarCPFPComRestricao() {

        getConsultaSeUmCPFPossuiOuNaoRestricaoRequest.retornaSeUmCPFPossuiOuNaoRestricaoRequest(cadastros.getcpfComRestricao())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("mensagem", is("O CPF " + cadastros.getcpfComRestricao() + " possui restrição"));
    }

    /*Método de teste responsável por consultar uma cpf sem restrição. */
    @Test
    @Category({AllTests.class})
    public void TC002_deveConsultarCPFPSemRestricao() {

        getConsultaSeUmCPFPossuiOuNaoRestricaoRequest.retornaSeUmCPFPossuiOuNaoRestricaoRequest(cadastros.getcpfSemRestricao())
                .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }
}
