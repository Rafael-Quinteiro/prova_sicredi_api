# Projeto Automatizado de Validação e Cadastro de Restrições e Simulações de crédito

O presente projeto visa mostrar casos de testes de validação e cadastro de Restrições e Simulações de crédito.

## 🚀 Começando

Essas instruções permitirão que você obtenha uma cópia do projeto em operação na sua máquina local para fins de teste.

### 📋 Pré-requisitos

De quais coisas você precisa instalar para conseguir executar os testes?

```
- Editor de código fonte;
- Maven
- JUnit
- RestAssured

```

## ⚙️ Executando os testes

Para obter êxito na execução dos testes, baixe o projeto em sua máquina, abra-o com seu editor de código e execute a classe de testes chamada "AllTests", que executará todos os testes.

### 🔩 Resumo do Teste Case

```
Os testes tem como objetivo validar a cadastrar restrições e simulações de crédito.

```

## 🛠️ Construído com

* [Maven](https://maven.apache.org/) - Gerenciador de Dependências
* [JUnit](https://junit.org/junit5/) - Framework
* [RestAssured](https://rest-assured.io/) - Framework
* [Java](https://www.java.com/pt-BR/) - Linguagem de Programação

## ✒️ Autor

* **Analista de Testes e Qualidade de Software (QA)** - [GitHub](https://github.com/Rafael-Quinteiro)

---
⌨️ com ❤️ por [Rafael De Oliveira Quinteiro](https://www.linkedin.com/in/rafael-de-oliveira-quinteiro-7693b8211/) 😊